import React from 'react';
import {createStore, applyMiddleware, combineReducers, compose} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import * as reducers from './reducers';
import Root from './Root';
import {ThemeProvider} from 'react-native-elements';

const createStoreWithMiddleware = applyMiddleware(thunk);
const reducer = combineReducers(reducers);
const store = createStore(reducer, undefined, createStoreWithMiddleware);


export default class App extends React.Component {

    render() {
        return (
            <Provider store={store}>
                <ThemeProvider theme={theme}>
                    <Root/>
                </ThemeProvider>
            </Provider>
        );
    }
}

const theme = {
    Card: {
        title: {
            color: "#ddd",
            flex: 1
        },
        titleStyle: {
            color: "#bbb",
            textAlign: 'left'
        }
    },
};
