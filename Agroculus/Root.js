import React from "react";
import {createDrawerNavigator, DrawerItems} from "react-navigation";
import Videos from './screens/Videos';
import Player from "./screens/Player";
import Settings from './screens/Settings';
import {ScrollView, View, Image, StyleSheet} from "react-native";

const AppStack = createDrawerNavigator({
        Videos: Videos,
        Player: Player,
        Settings: Settings
    },
    {
        contentComponent: props =>
            <ScrollView style={styles.menu}>
                <View style={styles.logo}>
                    <Image source={require('./assets/images/agroculus-01.png')}
                           style={styles.image}
                           resizeMode="contain"/>
                </View>
                <DrawerItems style={styles.items}{...props} />
            </ScrollView>,
        drawerWidth: 300
    });

export default class Root extends React.Component {
    render() {
        return <AppStack/>;
    }
}


const styles = StyleSheet.create({
    menu: {
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column'
    },
    image: {
        alignSelf: 'center',
        height: 150,
        width: 150,
    },
    logo: {
        width: '100%',
        backgroundColor: '#18599D',
    },
    items: {
        marginStart: 300,
        fontSize: 30
    }
});
