import React from 'react';
import {Text, StyleSheet, View, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Card, Header, Rating} from 'react-native-elements';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import Nyt360Video from 'react-native-nyt-360-video';

export class Player extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            paused: false
        }
    }

    // hide in drawer
    static navigationOptions = {
        drawerLabel: () => null
    };

    render() {
        return (
            <View>
                <Header leftComponent={<Text onPress={() => this.props.navigation.navigate('Videos')}
                                             style={styles.back}>‎←</Text>}
                        centerComponent={{text: 'View', style: {color: '#fff', fontSize: 18}}} />

                <Nyt360Video
                    ref={ref => this.player = ref}
                    style={{
                        width: Dimensions.get('screen').width,
                        height: 200,
                        backgroundColor: '#000000'
                    }}
                    source={{
                        uri: this.props.navigation.getParam('uri'),
                        type: this.props.navigation.getParam('type')
                    }}
                    displayMode={'embedded'}
                    volume={1.0}
                    paused={this.state.paused}
                    onLoad={(e) => console.log(e)}
                    onError={(e) => console.log(e)}
                    onEnd={(e) => console.log(e)}
                    onProgress={(e) => console.log(e)}
                    onPress={(e) => this.setState({paused: !this.state.paused})}
                    enableFullscreenButton={true}
                    enableCardboardButton={false}
                    enableTouchTracking={false}
                    hidesTransitionView={false}
                    enableInfoButton={false}/>

                <Card title="Description">
                    <Text style={styles.description}>
                        {this.props.navigation.getParam('description')}
                    </Text>
                </Card>

                <Rating showRating fractions={1.0} startingValue={3.3} />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {

    },
    back: {
        fontSize: 36,
        color: 'white'
    },
    description: {
        fontSize: 16,
        color: 'grey'
    },
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
    highlight: {
        fontWeight: '700',
    },
    footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
    },
});

export default Player;
