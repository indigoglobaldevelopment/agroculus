import React from 'react';
import {
    ScrollView,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Card, Button, Header, Rating} from 'react-native-elements';


export default class Settings extends React.Component {

    static navigationOptions = {
        drawerLabel: 'Settings',
        drawerIcon: ({tintColor}) => (<Icon name='gear' size={28} color={tintColor}/>)
    };

    render() {
        return (
            <View style={styles.container}>
                <Header leftComponent={<Text onPress={() => this.props.navigation.openDrawer()}
                                             style={styles.hamburger}>‎☰</Text>}
                        leftComponent={{onPress: () => this.props.navigation.openDrawer(), icon: 'menu', size: 32, color: '#fff'}}
                        centerComponent={{text: 'Settings', style: {color: '#fff', fontSize: 18}}} />

                <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                    <Card title="Setting">
                        <Text style={styles.description}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit
                        </Text>
                    </Card>
                    <Rating showRating fractions={1.0} startingValue={4.6} />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    contentContainer: {
        paddingTop: 30,
    },
    hamburger: {
        fontSize: 28,
        color: 'white'
    },
    description: {
        fontSize: 16,
        color: 'grey'
    }
});
