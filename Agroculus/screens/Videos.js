import React from 'react';
import {
    ScrollView,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Card, Button, Header, ListItem} from 'react-native-elements';


export default class Videos extends React.Component {

    static navigationOptions = {
        drawerLabel: 'Videos',
        drawerIcon: ({tintColor}) => (<Icon name='film' size={28} color={tintColor}/>)
    };


    view(item) {
        this.props.navigation.navigate('Player', item);
    }

    render() {
        return (
            <View style={styles.container}>
                <Header leftComponent={<Text onPress={() => this.props.navigation.openDrawer()}
                                             style={styles.hamburger}>‎☰</Text>}
                        centerComponent={{text: 'Videos', style: {color: '#fff', fontSize: 18}}} />

                <ScrollView style={styles.container}
                            contentContainerStyle={styles.contentContainer}>


                    {
                        list.map((l, i) => (
                            <ListItem onPress={() => {this.view(l)}}
                                key={i}
                                leftAvatar={{ source: { uri: 'http://www.pngmart.com/files/1/Video-Icon-PNG-HD.png'} }}
                                title={l.title}
                                subtitle={l.duration}
                                bottomDivider
                            />
                        ))
                    }
                </ScrollView>
            </View>
        );
    }
}

const list = [
    {
        title: 'Diving',
        duration: '3.28',
        uri: 'https://d2v9y0dukr6mq2.cloudfront.net/video/preview/eG7t61g/underwater-coral-reef-360-vr_S94kBUa0__WM.mp4',
        type: 'embedded',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
    },
    {
        title: 'progressive',
        duration: '2.17',
        uri: 'https://bitmovin.com/player-content/playhouse-vr/progressive.mp4',
        type: 'cardboard',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
    },
    {
        title: 'Gallery',
        duration: '0.0',
        uri: 'http://krystalboehlert.com/wp-content/uploads/2015/12/IMG_20151205_185511.vr_.jpg',
        type: 'cardboard',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
    }
];


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    contentContainer: {
        paddingTop: 30,
    },
    item: {
        fontSize: 36
    },
    hamburger: {
        fontSize: 28,
        color: 'white'
    }
});
